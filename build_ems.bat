@echo off
setlocal
if not exist "build" mkdir "build"
pushd build
if not exist "ems" mkdir "ems"
pushd ems

rem RelWithDebInfo
cmake -DWASM=1 -DCMAKE_TOOLCHAIN_FILE=%EMSCRIPTEN%\cmake\Modules\Platform\Emscripten.cmake -DCMAKE_BUILD_TYPE=Release -G "MinGW Makefiles" ../..

rem VERBOSE=1
mingw32-make.exe -j8

popd
popd
xcopy /y /E /F /D "working\web" "w:\dev\www\bomberman\web\"
xcopy /y /E /F /D "working\data" "w:\dev\www\bomberman\assets\"

rem cleanup
cd 
exit /b