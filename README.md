# Bomberman
C++ bomberman game.  
Controls: WASD or arrow keys - move, space - plant bomb, '1' (regular not numpad) -  spawn regular enemy, '2' - spawn following enemy, 'r' - restart level.  
Currently there is only one level and no win condition, so its just a sandbox.   
**
Live WebGL demo: http://deins.lv/bomberman/  
Download (windows): https://bitbucket.org/deins/bomberman/downloads/bomberman_win_x86.zip 
**

![alt text](https://bitbucket.org/deins/bomberman/raw/a4c970444b10fc9cec311a5957133c8a653d251e/screenshot.png "Screenshot")  

## Build instructions:
```sh
    git clone --recursive https://deins@bitbucket.org/deins/bomberman.git
	mkdir build
	cd build
	cmake ../ 
	# depending on OS and compiler call make or open project in IDE
```
	
## Art credits:  
Jacob Zinman-Jeanes (https://gamedevelopment.tutsplus.com/articles/enjoy-these-totally-free-bomberman-inspired-sprites--gamedev-8541)  
bevouliin.com (zombie sprites) (https://opengameart.org/content/bevouliin-free-zombie-sprite-sheets-game-character-for-game-developers)  