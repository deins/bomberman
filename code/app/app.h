#pragma once
#include "common.h"
#include "SDL_video.h"
#include "input.h"
#include "level.h"
#include <string>


class App 
{
	bool window_focus = false;
	Vec2i window_size = { 1024, 720 };
	SDL_Window* window = nullptr;
	SDL_GLContext gl_context = {};
	bool is_running = false;
	bool is_init = false;

	Level level;

#ifdef EMSCRIPTEN
	static void EmscriptenLoop();
#else
	void MainLoop();
#endif
	void ProcessFrame(float delta_t);
public:
	App() = default;
	~App();
	Input input;

	void Init();
	void Start();
	void OnEachFrame(float delta_t);
	FORCE_INLINE Vec2i GetWindowSize() { return window_size; }

	// returned string can contain also any binary values (it can act similarly to vector<uint_8>)
	std::string GetAsset(std::string path);
};