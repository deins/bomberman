#include "sprite.h"
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <memory>
#include <utility>
#include <ujdecode.h>
#include "app.h"

SpriteLibrary gSprites; // extern declared at common.h

void Sprite::FlipHorizontally()
{
	std::swap(uv[0], uv[1]);
	std::swap(uv[2], uv[3]);
}

bool SpriteLibrary::LoadSpriteSheet(std::string path)
{
	textures.emplace_back();
	Texture& texture = textures.back(); // TODO: delete texture on sprite sheet loading error
	LOG("Loading sprite sheet '%s'", path.c_str());
	{	// Load texture
		std::string file = gApp.GetAsset(path + ".png"); // TODO: different image types ? 
		Vec2i size;

		// Load image
		// WARNING: Texture is loaded vertically flipped. ( Top Left pixel is {0;0} )
		int img_components;
		RGBA* pixels = (RGBA*)stbi_load_from_memory((const uint8_t*)file.c_str(), file.size(), &size[0], &size[1], &img_components, 4);
		if (!pixels || size[0] == 0 || size[1] == 0)
		{
			FATAL_ERROR("Failed to load image! Reason: '%s'", stbi_failure_reason());
			return false;
		}
		texture.UploadTexture(pixels, size);
		STBI_FREE(pixels);
		LOG_DEBUG("gl texture: %d", texture.GetGLHandle());
	}
	{	// Load and parse sprite sheet description
		std::string file = gApp.GetAsset(path + ".json");
		void* state;
		UJObject obj = UJDecode(file.c_str(), file.size(), nullptr, &state); // WARNING: do not throw in this scope
		if (!obj)
		{
			FATAL_ERROR("Can't parse sprite sheet!");
			return false;
		}

		UJString key;
		UJObject value;
		auto* sprite = UJBeginObject(obj);
		while (UJIterObject(&sprite, &key, &value) != 0)
		{
			Vec2 source_size;
			Vec2 pos;
			Vec2 size;
			char name[128] = { 0 };
			for (unsigned i = 0; i < key.cchLen; ++i)
				name[i] = (char) key.ptr[i];
			LOG_DEBUG("Sprite '%s'", name);

			const wchar_t* sprite_keys[] = { L"frame", L"sourceSize", L"rotated" };
			UJObject o_frame, o_source_size, o_rotated;
			if (UJObjectUnpack(value, 3, "OOB", sprite_keys, &o_frame, &o_source_size, &o_rotated) == 3)
			{
				// TODO: implement rotated sprites
				const wchar_t* frame_keys[] = { L"x", L"y", L"width", L"height" };
				UJObject o_x, o_y, o_width, o_height;
				if (UJObjectUnpack(o_frame, 4, "NNNN", frame_keys, &o_x, &o_y, &o_width, &o_height) == 4)
				{
					pos.x =  (float) UJNumericFloat(o_x);
					pos.y =  (float) UJNumericFloat(o_y);
					size.x = (float) UJNumericFloat(o_width);
					size.y = (float) UJNumericFloat(o_height);
				}
				else FATAL_ERROR("Invalid sprite sheet description format!");

				Vec2 uv[4];
				ASSERT(sprites.count(name) == 0);
				Sprite& created_sprite = sprites[name];
				created_sprite.size = size; // TODO: maybe use sourceSize
				created_sprite.texture = texture.GetGLHandle();

				{	// uv
					// remember: texture is loaded vertically flipped
					pos /= (Vec2)texture.GetSize();
					size /= (Vec2)texture.GetSize();
					Vec2 border = Vec2(.5f) / (Vec2)texture.GetSize(); // hack! TODO: preprocess spritesheet to fix texture linear interpolation artifacts
					created_sprite.uv[0] = pos + Vec2(0 + border.x, size.y - border.y);
					created_sprite.uv[1] = pos + Vec2(size.x-border.y, size.y-border.x);
					created_sprite.uv[2] = pos + Vec2(size.x-border.x, 0 + border.y);
					created_sprite.uv[3] = pos + Vec2(0.0f) + border;
				}
			}
			else FATAL_ERROR("Invalid sprite sheet description format!");

			
			LOG_DEBUG("frame pos{%f %f} size{%f %f}", pos.x, pos.y, size.x, size.y);
		}

		UJFree(state);
	}
	return true;
}