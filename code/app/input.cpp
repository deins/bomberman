#include "input.h"

bool Input::ProcessEvent(SDL_Event e)
{
	switch (e.type)
	{
	case(SDL_KEYDOWN):
	{
		if (e.key.repeat == 0) {
			any_key_press = true;
			const unsigned code = e.key.keysym.scancode;
			if (code < MAX_KEYBOARD_KEYS)
				keys[code] = ButtonState::OnPress;
		}
		break;
	}

	case(SDL_KEYUP):
	{
		const unsigned code = e.key.keysym.scancode;
		if (code < MAX_KEYBOARD_KEYS)
			keys[code] = ButtonState::OnRelase;
		return true;
	}
	}
	return false;
}

void Input::Update()
{
	any_key_press = false;
	for (int i = 0; i < MAX_KEYBOARD_KEYS; ++i)
	{
		if (keys[i] == ButtonState::None)
			continue;
		if (keys[i] == ButtonState::OnPress)
			keys[i] = ButtonState::OnHold;
		else if (keys[i] == ButtonState::OnRelase)
			keys[i] = ButtonState::None;
	}
}

Input::ButtonState Input::GetKeyState(SDL_Scancode scancode)
{
	if (scancode >= (int)MAX_KEYBOARD_KEYS)
		return Input::ButtonState::None;
	return keys[scancode];
}

bool Input::IsKeyDown(SDL_Scancode scancode)
{
	auto state = GetKeyState(scancode);
	return state == Input::ButtonState::OnHold || state == Input::ButtonState::OnPress;
}

