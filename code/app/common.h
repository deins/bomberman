#pragma once
#include <cstdint>
#include <cfloat>
#include <cstdio>

#ifdef EMSCRIPTEN
#include <emscripten.h>
#endif

// ======================== MACROS ========================
//#ifdef _DEBUG
#ifdef __linux__
#include <signal.h>
#define DEBUG_BREAK() raise(SIGTRAP)
#elif _WIN32
#define DEBUG_BREAK() __debugbreak()
#elif EMSCRIPTEN
#define DEBUG_BREAK() __builtin_trap()
#else
#warning Platform not supporting DEBUG_BREAK!
#define DEBUG_BREAK() 
#endif
//#else
//#define DEBUG_BREAK() do{}while(0)
//#endif

#ifdef _WINDOWS 
#define FORCE_INLINE __forceinline
#elif defined(__GNUC__) || defined(EMSCRIPTEN)
#define FORCE_INLINE __attribute__((always_inline))
#else
#warning Platform doesnt support FORCE_INLINE or it isnt implemented!
#define FORCE_INLINE inline 
#endif

// ======================== LOGING AND DEBUGGING ========================
#include <SDL_log.h>
#define LOG_DEBUG(str, ...) 	do{ SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG,	str, ##__VA_ARGS__); }while(0)
#define LOG(str, ...) 			do{ SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO,		str, ##__VA_ARGS__); }while(0)
#define LOG_WARNING(str, ...) 	do{ SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_WARN,		str, ##__VA_ARGS__); DEBUG_BREAK(); }while(0)
#define LOG_ERROR(str, ...) 	do{ SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_ERROR,	str, ##__VA_ARGS__); DEBUG_BREAK(); }while(0)
#define FATAL_ERROR(str, ...) 	do{ SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_CRITICAL, str, ##__VA_ARGS__); DEBUG_BREAK(); abort(); }while(0)

#ifdef _DEBUG
#define ASSERT(x)				do{}while(0)
#else
#define ASSERT(x) 				do{if (!(x)) { FATAL_ERROR("ASERT(%s) FAILED", #x); } }while(0)
#endif
#define TODO_IMPLEMENT(MSG) FATAL_ERROR("TODO: IMPLEMENT!" MSG);

// ======================== MATH ========================
#define MATHFU_COMPILE_FORCE_PADDING 0	// let Vec3 be 3 floats, use vec4 by hand when SSE is needed
#ifdef EMSCRIPTEN
#define MATHFU_COMPILE_WITHOUT_SIMD_SUPPORT 1
#endif
#include <mathfu/utilities.h>
#include <mathfu/constants.h>
#include <mathfu/rect.h>
#if defined(EMSCRIPTEN) && defined(MATHFU_COMPILE_WITH_SIMD)
#error EMSCRIPTEN SIMD IS UNUSABLY SLOW WITHOUT BROWSER SUPPORT (https://github.com/kripken/emscripten/issues/4090)
#endif 
typedef mathfu::VectorPacked<float, 2> Vec2Packed;
typedef mathfu::VectorPacked<float, 3> Vec3Packed;
typedef mathfu::VectorPacked<float, 4> Vec4Packed;
typedef mathfu::Vector<float, 2> Vec2;
typedef mathfu::Vector<float, 3> Vec3;
typedef mathfu::Vector<float, 4> Vec4;
typedef mathfu::Vector<int, 2> Vec2i;
typedef mathfu::Vector<int, 3> Vec3i;
typedef mathfu::Vector<int, 4> Vec4i;
typedef mathfu::Quaternion<float> Quaternion;
typedef mathfu::Matrix<float, 3, 3> Mat3;
typedef mathfu::Matrix<float, 4, 4> Mat4;
typedef mathfu::Vector<uint8_t, 3> RGB;
typedef mathfu::Vector<uint8_t, 4> RGBA;
typedef mathfu::Rect<float> Rect;

#ifndef INFINITY
#define std::numeric_limits<float>::infinity()
#endif
#define VEC3_INFINITY Vec3{INFINITY,INFINITY,INFINITY}
#define VEC3_NEG_INFINITY Vec3{-INFINITY,-INFINITY,-INFINITY}

constexpr float PI = 3.14159265359f;
constexpr float DEG2RAD = PI / 180.0f;
constexpr float RAD2DEG = 180.0f / PI;

// ======================== COLORS ========================
namespace color {
	// 16 basic colors + transparent
	const RGBA Transparent(0, 0, 0, 0);
	const RGBA TransparentWhite(255, 255, 255, 0);
	const RGBA White(255, 255, 255, 255);
	const RGBA Black(0, 0, 0, 255);
	const RGBA Red(255, 0, 0, 255);
	const RGBA Lime(0, 255, 0, 255);
	const RGBA Blue(0, 0, 255, 255);
	const RGBA Yellow(255, 255, 0, 255);
	const RGBA Orange(255, 150, 0, 255);
	const RGBA Cyan(0, 255, 255, 255);
	const RGBA Pink(255, 0, 255, 255);
	const RGBA Silver(192, 192, 192, 255);
	const RGBA Grey(128, 128, 128, 255);
	const RGBA Maroon(128, 0, 0, 255);
	const RGBA Olive(128, 128, 0, 255);
	const RGBA Green(0, 128, 0, 255);
	const RGBA Purple(128, 0, 128, 255);
	const RGBA Teal(0, 128, 128, 255);
	const RGBA Navy(0, 0, 128, 255);
}

// ======================== GLOBAL VARIABLES ========================
class App;
class Gfx;
class SpriteLibrary;
extern App gApp;
extern Gfx gGfx;
extern SpriteLibrary gSprites;
