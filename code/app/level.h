#pragma once
#include <vector>
#include "common.h"
#include "sprite.h"

// starts with walkable tiles ends with non-walkable ones (for fast walkability check)
enum class Tile : uint8_t
{
	FLOOR,
	FIRE, // runtime collision-tile type, don't use directly as tile

	NON_WALKABLE_, // fake seperator tile type
	WALL,	// walls are indestructable
	BLOCK,	// destructable blocks
	POWERUP_SPEED, // hidden behind block 
	POWERUP_RANGE, // hidden behind block 
	POWERUP_BOMBS, // hidden behind block 
	BOMB,	// runtime collision-tile type, don't use directly as tile
	   	 
	SIZE_
};

enum class Direction : uint8_t
{
	N,E,S,W,
	SIZE_
};

struct Enemy
{
	enum Type
	{
		WANDERER,
		FOLLOWER,

		SIZE_
	} type = FOLLOWER;

	float speed = 2.0f;
	Vec2 pos;
	Vec2 target_pos;
	std::vector<Vec2i> path;
};

struct Player
{
	//int bombs;
	float speed = 2.0f;
	Vec2 pos;
	Direction dir = Direction::S; 
	bool is_moving = false;
	uint8_t bomb_range = 2;
	float death_timer = INFINITY;
};

struct Bomb
{
	Vec2 pos; 
	float timer;
	uint8_t range = 1; // explosion range in tiles
};

struct Fire
{
	Vec2 pos;
	float timer;
};

struct Powerup
{
	enum Type
	{
		SPEED,
		RANGE,
		BOMBS,
		SIZE_
	} type;
	Vec2 pos;
};

struct Decal
{
	Sprite sprite;
	Vec2 pos;
	Vec2 size;
	float health = 1.0f;
};

class Level
{
	static constexpr float BOMB_TIMER = 4.0f; //seconds
	static constexpr float FIRE_TIMER = .95f; //seconds

	struct AStarTile
	{
		float distance = INFINITY;
		Vec2i backtrack;
	};

	float time; // simulation time
	Vec2i size = Vec2i(0); // world size
	std::vector<Tile> tiles;
	std::vector<AStarTile> astar_tiles;
	std::vector<Bomb> bombs;
	std::vector<Fire> fires;
	std::vector<Enemy> enemies;
	std::vector<Powerup> powerups;
	std::vector<Decal> decals;
	Player player;
	std::string current_level;
	bool restart = false;

	struct Sprites
	{
		Sprite tiles[(unsigned)Tile::SIZE_];
		Sprite bomb, fire;
		Sprite player[(unsigned)Direction::SIZE_][8];
		Sprite enemy[(unsigned)Enemy::Type::SIZE_][2];
		Sprite enemy_dead[(unsigned)Enemy::Type::SIZE_];
		Sprite powerup[(unsigned)Powerup::Type::SIZE_];
	} sprites;

	FORCE_INLINE AStarTile& GetAStarTile(Vec2i);
public:
	FORCE_INLINE bool IsValidTile(Vec2i pos) const;
	FORCE_INLINE Tile GetTile(Vec2i) const;
	FORCE_INLINE Tile& GetTileRef(Vec2i);
	FORCE_INLINE void SetTile(Vec2i, Tile);
	FORCE_INLINE bool IsTileWalkable(Vec2i);
	bool Load(const char* path);
	void OnEachFrame(float delta_t);
	void Draw();
	std::vector<Vec2i> FindPath(Vec2i start, Vec2i goal); // NOTE: returned path is in reverse order! It is done on purpouse so that you can call pop_back for easy path folowing
	Vec2 FindEnemySpawnPoint();
	Enemy& CreateEnemy(Vec2 pos, Enemy::Type type);
	void DestroyBlock(Vec2i pos);
	void KillPlayer();
};