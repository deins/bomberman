#pragma once
#include <string>
#include <unordered_map>
#include <deque>
#include <glad.h>
#include "common.h"
#include "texture.h"

struct Sprite
{
	GLuint texture = 0;
	Vec2 size;
	Vec2 uv[4];

	void FlipHorizontally();
};

class SpriteLibrary
{	
	std::unordered_map<std::string, struct Sprite> sprites;
	std::deque<Texture> textures; // list of all sprite sheet textures. WARNING: data structure can't reallocate textures due to Texture destructor!
public:
	// note: path is without extension because SpriteSheet consists of 2 files (.json description and image)
	bool LoadSpriteSheet(std::string path);
	FORCE_INLINE Sprite& Get(const char* name) { ASSERT(sprites.count(name) == 1);  return sprites[name]; }
};

