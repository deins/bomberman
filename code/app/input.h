#pragma once
#include <vector>
#include <SDL_events.h>
#include <SDL_mouse.h>
#include <SDL_keyboard.h>
#include "common.h"

class Input
{
public:
	enum class MouseButton
	{
		Left = SDL_BUTTON_LEFT,
		Right = SDL_BUTTON_RIGHT,
		Middle = SDL_BUTTON_MIDDLE
	};

	enum class ButtonState
	{
		None,
		OnPress,
		OnHold,
		OnRelase
	};
private:
	static constexpr unsigned MAX_KEYBOARD_KEYS = SDL_NUM_SCANCODES;
	static constexpr unsigned MAX_MOUSE_BUTTONS = 16;
	bool any_key_press = false;
	ButtonState keys[MAX_KEYBOARD_KEYS] = {};
	//ButtonState mouse_buttons[MAX_MOUSE_BUTTONS] = {};
public:

	bool ProcessEvent(SDL_Event e); // return true if event was consumed
	void Update();

	bool IsKeyDown(SDL_Scancode);
	ButtonState GetKeyState(SDL_Scancode);
};