#include "gfx.h"
#include "shaders.inl.h"
#include "app.h"
#include "sprite.h"

void Gfx::Init()
{
	LOG("Gfx init");
	if (!default_shader.CompileShader(default_shader_vs, default_shader_fs))
		FATAL_ERROR("Can't compile shader!");
	default_shader.Bind();
	current_shader = &default_shader;
	LOG("Shaders ok.");

	vertex_buffer.reserve(1 << 16);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_SCISSOR_TEST);

#ifndef EMSCRIPTEN
	GLuint va;
	glGenVertexArrays(1, &va);
	glBindVertexArray(va);
#endif

	// TODO: don't use alpha blending where it is not nececary
	glEnable(GL_BLEND); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	{	// Generate quad indices (GL_QUADS is not available on OpenGL ES and in core profile)
		GLuint buffer;
		glGenBuffers(1, &buffer); // TODO: batch allocate on init
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
		std::vector<uint16_t> indices(MAX_INDICES);
		for (int idx = 0, i = 0; i <= MAX_INDICES - 6; idx += 4)
		{
			indices[i++] = (idx	 );
			indices[i++] = (idx + 1);
			indices[i++] = (idx + 2);
			indices[i++] = (idx	 );
			indices[i++] = (idx + 2);
			indices[i++] = (idx + 3);
		}
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_INDICES * sizeof(uint16_t), indices.data(), GL_STATIC_DRAW);
		GL_DEBUG_CHECK();
	}
}

void Gfx::BeginFrame()
{
	GL_DEBUG_CHECK();
	Vec2i win_size_int = gApp.GetWindowSize();
	Vec2 win_size = (Vec2)win_size_int;
	glViewport(0, 0, win_size_int.x, win_size_int.y);
	glClearColor(background_color[0] / 255.0f, background_color[1] / 255.0f, background_color[2] / 255.0f, background_color[3] / 255.0f);
	glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT*/);
	vertex_buffer.clear();
	color = color::Grey;

	//Vec2 uv_[4] = { {0.0f, 0.0f}, { 1.0f,0.0f }, { 1.0f,1.0f }, { 0.0f,1.0f } };
	//memcpy(uv, uv_, sizeof(uv)); // TODO: remove
}

void Gfx::EndFrame()
{
	DrawBuffer();
	GL_DEBUG_CHECK();
}

void Gfx::SetRenderParameters(RenderParameters p)
{
	DrawBuffer();
	mvp = p.mvp;
}

void Gfx::SetColor(RGBA col)
{
	color = col;
}

void Gfx::SetAlpha(float a)
{
	if (a >= 1.0f) color[3] = 255;
	else if (a <= 0.01f) color[3] = 0;
	else color[3] = (uint8_t)(a * 255);
}

void Gfx::DrawQuad(Vec2 center, Vec2 extents)
{
	Vec3 p[4]{
		{center.x - extents.x, center.y - extents.y, 0.0f},
		{center.x + extents.x, center.y - extents.y, 0.0f},
		{center.x + extents.x, center.y + extents.y, 0.0f},
		{center.x - extents.x, center.y + extents.y, 0.0f},
	};
	for (int i = 0; i < 4; ++i)
		vertex_buffer.push_back({ p[i], color, uv[i] });
}

void Gfx::SetSprite(const Sprite& sprite)
{
	if (Texture::GetBoundTexture() != sprite.texture)
	{
		DrawBuffer(); // change texture
		Texture::Bind(sprite.texture);
	}

	static_assert(sizeof(uv) == sizeof(sprite.uv), "mismatching sizes");
	memcpy(uv, sprite.uv, sizeof(uv));
}

void Gfx::DrawSprite(const Sprite& sprite, Vec2 pos, Vec2 size)
{
	SetSprite(sprite);
	DrawQuad(pos, size * .5f);
}

void Gfx::DrawSprite(const Sprite & sprite, Vec2 center_pos)
{
	DrawSprite(sprite, center_pos, sprite.size);
}

void Gfx::DrawBuffer()
{
	if (vertex_buffer.size() == 0)
		return;

	GLuint buffer;
	glGenBuffers(1, &buffer); // TODO: batch allocate on init
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, vertex_buffer.size() * sizeof(Vertex), vertex_buffer.data(), GL_STREAM_DRAW);

	// Attributes
	// TODO: assign static attribute locations in shader or at shader linking stage
	int pos_loc = current_shader->GetAttributeLocation("Position");
	int col_loc = current_shader->GetAttributeLocation("Color");
	int uv_loc = current_shader->GetAttributeLocation("Uv");
	glEnableVertexAttribArray(pos_loc);
	glEnableVertexAttribArray(col_loc);
	glEnableVertexAttribArray(uv_loc);
	glVertexAttribPointer(pos_loc, 3, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, pos));
	glVertexAttribPointer(col_loc, 4, GL_UNSIGNED_BYTE, true, sizeof(Vertex), (void*)offsetof(Vertex, color));
	glVertexAttribPointer(uv_loc, 2, GL_FLOAT, false, sizeof(Vertex), (void*)offsetof(Vertex, uv));

	// Uniforms
	GLint mvp_loc = current_shader->GetUniformLocation("MVP");
	ASSERT(mvp_loc != -1);
	Vec4Packed packed[4];
	mvp.Pack(packed);
	glUniformMatrix4fv(mvp_loc, 1, false, packed->data);

	//GLint tex_0_loc = current_shader->GetUniformLocation("TEXTURE_0");
	//ASSERT(tex_0_loc != -1);
	//glUniform1i(tex_0_loc, 1);
	//GL_DEBUG_CHECK();

	//glDrawArrays(GL_QUADS, 0, vertex_buffer.size());
	glDrawElements(GL_TRIANGLES, vertex_buffer.size() * 6 / 4, GL_UNSIGNED_SHORT, 0);
	GL_DEBUG_CHECK();
	glDeleteBuffers(1, &buffer); // TODO: reuse buffer handles
	GL_DEBUG_CHECK();
	vertex_buffer.clear();
}
