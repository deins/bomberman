#include <cstdio>
#include "common.h"
#include "app.h"
#include "gfx.h"

App gApp;
Gfx gGfx;

#include <fstream>
int main(int argc, char* argv[]) 
{
	gApp.Init();
	gGfx.Init();
	gApp.Start();
	return 0;
}