#pragma once
#include "common.h"
#include <vector>
#include <glad.h>
#include <vector>
#include "shader.h"

class Image;
struct Sprite;

class Gfx
{
public:

	struct Vertex
	{
		Vec3 pos;
		RGBA color;
		Vec2 uv;
	};

	struct RenderParameters {
		Mat4 mvp; // model view projection matrix]
	};

	static constexpr unsigned MAX_INDICES = (1 << 16);
	GLuint quad_indices[MAX_INDICES];
	RGBA background_color = color::Black;

	void Init();
	void BeginFrame();
	void EndFrame();

	void SetRenderParameters(RenderParameters);
	void SetColor(RGBA col);
	void SetAlpha(float); // same as SetColor just with float data type
	FORCE_INLINE void ResetColor() { color = color::Grey; }
	void SetSprite(const Sprite& sprite);
	void DrawQuad(Vec2 center, Vec2 extents);
	void DrawSprite(const Sprite& sprite, Vec2 center_pos, Vec2 size);
	void DrawSprite(const Sprite& sprite, Vec2 center_pos);

	GLuint UploadTexture(Image& img, GLuint texture = 0);
private:
	RGBA color = RGBA(255, 128, 128, 255);
	Vec2 uv[4];

	Mat4 mvp; // model view projection matrix
	Shader* current_shader = nullptr;
	Shader default_shader;
	std::vector<Vertex> vertex_buffer;
	void DrawBuffer();
};

// Debug Macros
#define GL_CHECK() [](){GLenum e = glGetError(); if (e!=GL_NO_ERROR){\
		const char* msg = "Unknown";\
		switch (e){\
			case(GL_INVALID_ENUM) : msg = "GL_INVALID_ENUM"; break; \
			case(GL_INVALID_VALUE) : msg = "GL_INVALID_VALUE"; break; \
			case(GL_INVALID_OPERATION) : msg = "GL_INVALID_OPERATION"; break; \
			case(GL_OUT_OF_MEMORY) : msg = "GL_OUT_OF_MEMORY"; break; \
		}\
		LOG_ERROR("GL_ERROR: '%s'", msg); \
	}}()

#ifdef _DEBUG
#define	GL_DEBUG_CHECK() GL_CHECK()
#else
#define GL_DEBUG_CHECK() do{}while(0)
#endif

