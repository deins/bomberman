#pragma once
#include <vector>
#include <glad.h>
#include <string>

class Shader
{
public:
	/* Uniform & Attribute type values:
	GL_FLOAT, GL_FLOAT_VEC2, GL_FLOAT_VEC3, GL_FLOAT_VEC4, GL_INT, GL_INT_VEC2, GL_INT_VEC3, GL_INT_VEC4, GL_BOOL, GL_BOOL_VEC2, GL_BOOL_VEC3, GL_BOOL_VEC4, GL_FLOAT_MAT2, GL_FLOAT_MAT3, GL_FLOAT_MAT4, GL_SAMPLER_2D, or GL_SAMPLER_CUBE
	*/
	struct Attribute
	{
		std::string name;
		GLint location = -1;
		GLenum type = 0;
		int size; // array size
		bool operator < (const std::string&) const; // for binary search
		bool operator == (const std::string&) const; // for linear search
	};

	struct Uniform
	{
		std::string name;
		GLint location = -1;
		GLenum type;
		int size; // array size
		bool operator < (const std::string&) const; // for binary search
		bool operator == (const std::string&) const; // for linear search
	};

	Shader() = default;
	~Shader();

	bool CompileShader(const char* vertex_shader, const char* fragment_shader);
	GLint  GetUniformLocation(const std::string& name) const;
	GLint  GetAttributeLocation(const std::string& name) const;
	const std::vector<Uniform>& GetUniforms() const;
	const std::vector<Attribute>& GetAttributes() const;
	void Bind();
	static void Unbind();
	bool GetLinkStatus();
private:
	GLuint program = 0; // Shader program
	std::vector<Attribute> attribs;
	std::vector<Uniform> uniforms;
	static GLuint gBoundProgram;

	GLuint CreateFromSource(const char* source, GLenum type);
};