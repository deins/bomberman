#include "level.h"
#include "app.h"
#include "sprite.h"
#include "gfx.h"
#include <sstream>
#include <queue>

Vec2 dir_vector[4] = { {0.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, -1.0f}, {-1.0f, 0.0f} };
Vec2i dir_vector_i[4] = { {0, 1}, {1, 0}, {0, -1}, {-1, 0} };

bool Level::IsValidTile(Vec2i pos) const
{
	return pos.x >= 0 && pos.y >= 0 && pos.x < size.x && pos.y < size.y;
}

Tile Level::GetTile(Vec2i p) const
{
	ASSERT(IsValidTile(p));
	return tiles[p.x + p.y*size.x];
}

Tile& Level::GetTileRef(Vec2i p)
{
	ASSERT(IsValidTile(p));
	return tiles[p.x + p.y*size.x];
}

void Level::SetTile(Vec2i p, Tile tile)
{
	ASSERT(IsValidTile(p));
	tiles[p.x + p.y*size.x] = tile;
}

Level::AStarTile& Level::GetAStarTile(Vec2i p)
{
	ASSERT(IsValidTile(p));
	return astar_tiles[p.x + p.y*size.x];
}

bool Level::IsTileWalkable(Vec2i p)
{
	Tile t = GetTile(p);
	return t < Tile::NON_WALKABLE_;
	//return (t != Tile::BLOCK) && (t != Tile::WALL) && (t != Tile::BOMB);
}

bool Level::Load(const char * path)
{
	LOG("Loading level '%s'", path);
	current_level = path;
	std::string file_str = gApp.GetAsset(path);
	std::istringstream file(file_str.c_str());
	file >> std::skipws; // skip all whitespaces
	file >> size.x >> size.y;
	tiles.resize(size.x * size.y);
	for (int y = size.y - 1; y >= 0; --y)
		for (int x = 0; x < size.x; ++x)
		{
			char c;
			file >> c;
			switch (c)
			{
			case ('.'):
				SetTile({ x,y }, Tile::FLOOR);
				break;
			case ('X'):
				SetTile({ x,y }, Tile::WALL);
				break;
			case ('B'):
				SetTile({ x,y }, Tile::BLOCK);
				break;
			case ('P'):
				// Player Spawn
				player.pos = Vec2((int)x, (int)y) + Vec2(.5f);
				break;
			case('+'):
				CreateEnemy(Vec2((int)x, (int)y) + Vec2(.5f), Enemy::Type::WANDERER);
				break;
			case('*'):
				CreateEnemy(Vec2((int)x, (int)y) + Vec2(.5f), Enemy::Type::FOLLOWER);
				break;
			case('s'):
				SetTile({ x,y }, Tile::POWERUP_SPEED);
				break;
			case('b'):
				SetTile({ x,y }, Tile::POWERUP_BOMBS);
				break;
			case('r'):
				SetTile({ x,y }, Tile::POWERUP_RANGE);
				break;
			default:
				LOG_ERROR("Unknown tile '%c'!", c);
				SetTile({ x,y }, Tile::FLOOR);
				break;
			}
		}

	if (!file)
		LOG_ERROR("Can't load level '%s'!", path);

	// Initialization
	LOG("Loding sprites");
	memset(&sprites, 0xFF, sizeof(sprites));
	sprites.tiles[(unsigned)Tile::FLOOR] = gSprites.Get("Blocks/BackgroundTile");
	sprites.tiles[(unsigned)Tile::BLOCK] = gSprites.Get("Blocks/ExplodableBlock");
	sprites.tiles[(unsigned)Tile::WALL] = gSprites.Get("Blocks/SolidBlock");

	sprites.tiles[(unsigned)Tile::POWERUP_BOMBS] = gSprites.Get("Blocks/ExplodableBlock");
	sprites.tiles[(unsigned)Tile::POWERUP_RANGE] = gSprites.Get("Blocks/ExplodableBlock");
	sprites.tiles[(unsigned)Tile::POWERUP_SPEED] = gSprites.Get("Blocks/ExplodableBlock");

	sprites.tiles[(unsigned)Tile::BOMB] = sprites.tiles[(unsigned)Tile::FLOOR];
	sprites.tiles[(unsigned)Tile::FIRE] = sprites.tiles[(unsigned)Tile::FLOOR];

	sprites.bomb = gSprites.Get("Bomb/Bomb_f01");
	sprites.fire = gSprites.Get("Flame/Flame_f00");

	for (int i = 0; i < 8; ++i)
	{
		std::string num = std::to_string(i);
		sprites.player[(unsigned)Direction::N][i] = gSprites.Get(((std::string)"Bomberman/Back/Bman_B_f0" + num).c_str());
		sprites.player[(unsigned)Direction::E][i] = gSprites.Get(((std::string)"Bomberman/Side/Bman_S_f0" + num).c_str());
		sprites.player[(unsigned)Direction::S][i] = gSprites.Get(((std::string)"Bomberman/Front/Bman_F_f0" + num).c_str());
		sprites.player[(unsigned)Direction::W][i] = gSprites.Get(((std::string)"Bomberman/Side/Bman_S_f0" + num).c_str());
		sprites.player[(unsigned)Direction::W][i].FlipHorizontally();
	}

	sprites.enemy[0][0] = gSprites.Get("GreenZombie/frame_1");
	sprites.enemy[0][1] = gSprites.Get("GreenZombie/frame_2");
	sprites.enemy[1][0] = gSprites.Get("RedZombie/frame_1");
	sprites.enemy[1][1] = gSprites.Get("RedZombie/frame_2");
	sprites.enemy_dead[0] = gSprites.Get("GreenZombie/dead_1");
	sprites.enemy_dead[1] = gSprites.Get("RedZombie/dead_2");

	sprites.powerup[(unsigned)Powerup::SPEED] = gSprites.Get("Powerups/SpeedPowerup");
	sprites.powerup[(unsigned)Powerup::RANGE] = gSprites.Get("Powerups/FlamePowerup");
	sprites.powerup[(unsigned)Powerup::BOMBS] = gSprites.Get("Powerups/BombPowerup");

	return (bool)file;
}

void Level::OnEachFrame(float delta_t)
{
	if (restart)
	{
		std::string level = std::move(current_level);
		*this = Level();
		Load(level.c_str());
	}

	time += delta_t;
	{	// Player
		if (player.death_timer == INFINITY)
		{
			Vec2 mov_dir = Vec2(0.0f);
			if (gApp.input.IsKeyDown(SDL_SCANCODE_W) || gApp.input.IsKeyDown(SDL_SCANCODE_UP))
				++mov_dir.y;
			if (gApp.input.IsKeyDown(SDL_SCANCODE_S) || gApp.input.IsKeyDown(SDL_SCANCODE_DOWN))
				--mov_dir.y;
			if (gApp.input.IsKeyDown(SDL_SCANCODE_D) || gApp.input.IsKeyDown(SDL_SCANCODE_RIGHT))
				++mov_dir.x;
			if (gApp.input.IsKeyDown(SDL_SCANCODE_A) || gApp.input.IsKeyDown(SDL_SCANCODE_LEFT))
				--mov_dir.x;
			if (mov_dir.Normalize() > 0.0f)
			{
				Vec2 old_pos = player.pos;
				float mov_dist = player.speed * delta_t;
				player.pos += mov_dist * mov_dir;
				{// Collision 
					Vec2i pos_i(player.pos);
					float player_extents = .3f;
					{	// N
						Vec2i p = pos_i + dir_vector_i[0];
						if (!IsTileWalkable(p))
						{
							float diff = player.pos.y + player_extents - p.y;
							if (diff > 0.0f)
								player.pos.y -= diff;
						}
					}
					{	// E
						Vec2i p = pos_i + dir_vector_i[1];
						if (!IsTileWalkable(p))
						{
							float diff = player.pos.x + player_extents - p.x;
							if (diff > 0.0f)
								player.pos.x -= diff;
						}
					}
					{	// S
						Vec2i p = pos_i + dir_vector_i[2];
						if (!IsTileWalkable(p))
						{
							float diff = player.pos.y - player_extents - (p.y + 1);
							if (diff < 0.0f)
								player.pos.y -= diff;
						}
					}
					{	// W
						Vec2i p = pos_i + dir_vector_i[3];
						if (!IsTileWalkable(p))
						{
							float diff = player.pos.x - player_extents - (p.x + 1);
							if (diff < 0.0f)
								player.pos.x -= diff;
						}
					}
				}
				{	// Compensate collision response movement distance
					Vec2 dir = player.pos - old_pos;
					float displacement = dir.Normalize();
					if (displacement > 0.01f)
					{
						float diff = mov_dist - displacement;
						player.pos += dir * diff;
					}
				}

				// Sprite direction
				if (mov_dir.x > 0.1f) player.dir = Direction::E;
				else if (mov_dir.x < -0.1f) player.dir = Direction::W;
				else player.dir = mov_dir.y > 0 ? Direction::N : Direction::S;
				player.is_moving = Vec2::DistanceSquared(old_pos, player.pos) > .01f * delta_t;
			}
			else
				player.is_moving = false;

			if (gApp.input.GetKeyState(SDL_SCANCODE_SPACE) == Input::ButtonState::OnPress)
			{	// Plant bomb
				Vec2 pos = { std::floor(player.pos.x), std::floor(player.pos.y) };
				if (GetTile((Vec2i)pos) != Tile::BOMB)
				{
					SetTile((Vec2i)pos, Tile::BOMB);
					bombs.push_back({ pos, BOMB_TIMER, player.bomb_range });
				}
			}

			if (gApp.input.GetKeyState(SDL_SCANCODE_1) == Input::ButtonState::OnPress)
				CreateEnemy(FindEnemySpawnPoint(), Enemy::WANDERER);
			if (gApp.input.GetKeyState(SDL_SCANCODE_2) == Input::ButtonState::OnPress)
				CreateEnemy(FindEnemySpawnPoint(), Enemy::FOLLOWER);
			if (gApp.input.GetKeyState(SDL_SCANCODE_R) == Input::ButtonState::OnPress)
				restart = true;
		}

		// Die
		if (GetTile((Vec2i)player.pos) == Tile::FIRE)
			KillPlayer();
		if (player.death_timer != INFINITY)
		{
			player.death_timer -= delta_t;
			if (player.death_timer < 0)
				restart = true;
		}
	}

	{	// Bombs
		for (unsigned b = 0; b < bombs.size(); ++b)
		{
			Bomb& bomb = bombs[b];
			bomb.timer -= delta_t;
			if (bomb.timer <= 0.0f)
			{
				// Explosion
				auto SetOnFire = [&](Vec2i pos)
				{
					Tile t = GetTile(pos);
					DestroyBlock(pos);
					SetTile(pos, Tile::FIRE);
					fires.push_back({ (Vec2)pos, FIRE_TIMER });

					if (t == Tile::BOMB)
					{
						for (Bomb& b : bombs)
						{
							if (Vec2i(b.pos) == pos)
							{
								b.timer = -1.0f;
								break;
							}
						}
					}
				};
				Vec2i pos_i(bomb.pos);
				SetOnFire(pos_i);
				for (int i = 0; i < 4; ++i)
				{
					Vec2i dir = dir_vector_i[i];
					Vec2i current_pos = pos_i + dir;
					for (int i = 1; i < bomb.range; ++i, current_pos += dir)
					{
						Tile tile = GetTile(current_pos);
						if (tile == Tile::WALL) break;
						if (tile != Tile::FIRE)
							SetOnFire(current_pos);
						else
						{
							for (Fire& f : fires)
								if ((Vec2i)f.pos == current_pos)
								{
									f.timer = FIRE_TIMER;
									break;
								}
						}
						if (tile == Tile::BLOCK) break;
					}
				}
				// Remove bomb
				bombs[b] = bombs.back();
				bombs.pop_back();
				--b;
				continue;
			}
		}
	}

	{	// Enemies
		for (unsigned ei = 0; ei < enemies.size(); ++ei)
		{
			Enemy& e = enemies[ei];
			if (GetTile((Vec2i)e.pos) == Tile::FIRE)
			{	// walked on fire - destroy
				decals.push_back({ sprites.enemy_dead[(unsigned)e.type], e.pos, Vec2(1.0f), .8f });

				enemies[ei] = enemies.back();
				enemies.pop_back();

				--ei;
				continue;
			}

			if (Vec2::DistanceSquared(e.pos, player.pos) < .6f*.6f)
				KillPlayer();

			if (e.type == Enemy::FOLLOWER)
			{
				e.path = FindPath((Vec2i)e.pos, (Vec2i)player.pos); // TODO: optimize: don't search for path every frame
			}

			bool wander = e.path.size() == 0 || e.type == Enemy::WANDERER;
			{	// move to target pos
				bool target_occupied = !IsTileWalkable((Vec2i)e.target_pos);
				Vec2 diff = e.target_pos - e.pos;
				if (target_occupied || diff.LengthSquared() <= (e.speed * delta_t) * (e.speed * delta_t))
				{	// target pos reached or bomb was placed on target pos
					if (target_occupied) e.path.clear();
					e.pos = e.target_pos;
					if (wander)
					{
						Vec2i posi = (Vec2i)e.pos;
						Vec2i next_target[4];
						unsigned next_target_count = 0;
						for (int i = 0; i < 4; ++i)
						{
							Vec2i p = posi + dir_vector_i[i];
							if (IsTileWalkable(p))
							{
								next_target[next_target_count++] = p;
							}
						}
						if (next_target_count > 0)
							e.target_pos = (Vec2)next_target[rand() % next_target_count] + Vec2(.5);
					}
					else if (e.path.size() > 0)
					{
						e.target_pos = (Vec2)e.path.back() + Vec2(.5f);
						e.path.pop_back();
					}
				}
				else
					e.pos += diff.Normalized() * e.speed * delta_t;
			}
		}
	}

	{	// Fires
		for (unsigned f = 0; f < fires.size(); ++f)
		{
			fires[f].timer -= delta_t;
			if (fires[f].timer <= 0.0f)
			{	// remove
				SetTile((Vec2i)fires[f].pos, Tile::FLOOR);
				fires[f] = fires.back();
				fires.pop_back();
				--f;
			}
		}
	}

	// Powerups
	if (player.is_moving)
	{
		for (auto it = powerups.begin(); it != powerups.end();)
		{
			if (Vec2::DistanceSquared(player.pos, it->pos) < .5f*.5f)
			{
				Powerup::Type type = it->type;
				if (type == Powerup::SPEED)
					player.speed += .75f;
				else if (type == Powerup::RANGE)
					++player.bomb_range;
				it = powerups.erase(it);
				// TODO: bombs
			}
			else
				++it;
		}
	}

	// Decals
	for (Decal& d : decals)
		d.health -= delta_t;
	decals.erase(std::remove_if(decals.begin(), decals.end(), [](const Decal& d) {return d.health < 0.01; }), decals.end());

}

void Level::Draw()
{
	// Setup view
	const float aspect = (float)gApp.GetWindowSize().x / gApp.GetWindowSize().y;
	const float scale = 8.0f;
	Vec2 view_min = { -scale * aspect, -scale };
	Vec2 view_max = { scale * aspect, scale };
	view_min += player.pos;
	view_max += player.pos;

	{	// Align view to level boundaries
		Vec2 view_offset = Vec2(0.0f);
		if (view_max.x - view_min.x < size.x)
		{
			if (view_min.x < 0.0f) view_offset.x += -view_min.x;
			if (view_max.x > size.x) view_offset.x -= view_max.x - size.x;
		}
		else view_offset.x = -player.pos.x + size.x*.5f;
		if (view_max.y - view_min.y < size.y)
		{
			if (view_min.y < 0.0f) view_offset.y += -view_min.y;
			if (view_max.y > size.y) view_offset.y -= view_max.y - size.y;
		}
		else view_offset.y = -player.pos.y + size.y*.5f;
		view_min += view_offset;
		view_max += view_offset;
	}

	Mat4 mvp = Mat4::Ortho(view_min.x, view_max.x, view_min.y, view_max.y, -1.0f, 1.0f);
	gGfx.SetRenderParameters({ mvp });

	// Draw tilemap
	Vec2i tilemap_min = Vec2i::Max(Vec2i(0), (Vec2i)view_min);
	Vec2i tilemap_max = Vec2i::Min(size, (Vec2i)view_max + Vec2i(1));	
	for (int y = tilemap_min.y; y < tilemap_max.y; ++y)
		for (int x = tilemap_min.x; x < tilemap_max.x; ++x)
		{
			gGfx.DrawSprite(sprites.tiles[(unsigned)GetTile({ x, y })], Vec2((float)x, (float)y) + Vec2(.5f), Vec2(1.0f));
		}

	// TODO: optimize: cull sprites outside of view
	for (const Powerup& p : powerups)
	{
		gGfx.DrawSprite(sprites.powerup[(unsigned)p.type], p.pos, Vec2(.6f));
	}

	gGfx.SetAlpha(.6f);
	for (const Bomb& bomb : bombs)
	{
		gGfx.DrawSprite(sprites.bomb, bomb.pos + Vec2(.5f), Vec2(0.7f) + Vec2(sin(bomb.timer * 4)) * Vec2(.1f, .2f));
	}
	gGfx.ResetColor();

	int frame = (int)(time*4.0f) % 2;
	for (const Enemy& e : enemies)
	{
		gGfx.DrawSprite(sprites.enemy[(unsigned)e.type][frame], e.pos, Vec2(1.0f));
	}

	for (const Decal& decal : decals)
	{
		gGfx.SetAlpha(decal.health);
		gGfx.DrawSprite(decal.sprite, decal.pos, decal.size);
	}

	for (const Fire& fire : fires)
	{
		gGfx.SetAlpha(fire.timer * 2.0f);
		gGfx.DrawSprite(sprites.fire, fire.pos + Vec2(.5f), Vec2(0.7f) + Vec2(sin(fire.timer * 7), cos(fire.timer * 7)) * Vec2(.1f));
	}
	gGfx.ResetColor();

	{	// Player
		int frame = 0;
		if (player.death_timer < INFINITY)
			gGfx.SetAlpha(player.death_timer);
		else
		{
			if (player.is_moving)
				frame = (int)(time * 12.0 * player.speed) % 8;
		}
		gGfx.DrawSprite(sprites.player[(unsigned)player.dir][frame], player.pos + Vec2(.0f, .5f), Vec2(1.0f, 1.5f));
		gGfx.ResetColor();
	}
}

std::vector<Vec2i> Level::FindPath(Vec2i start, Vec2i goal)
{
	ASSERT(IsValidTile(start));

	astar_tiles.clear();
	astar_tiles.resize(tiles.size());

	struct Node
	{
		float score = INFINITY; // distance + H
		Vec2i pos;
		FORCE_INLINE bool operator < (const Node& rhs) const { return score < rhs.score; }
		FORCE_INLINE bool operator > (const Node& rhs) const { return score > rhs.score; }
	};

	std::priority_queue<Node, std::vector<Node>, std::greater<Node>> qu;
	qu.push({ 0.0f, start });
	GetAStarTile(start) = { 0.0f, start };

	int nodes_visited = 0;
	while (!qu.empty())
	{
		++nodes_visited;
		Node node = qu.top();
		qu.pop();

		//LOG_DEBUG("pop {%d; %d} dist=%f; score=%f; ", node.pos.x, node.pos.y, GetAStarTile(node.pos), node.score);
		if (node.pos == goal) break; // WARNING: this is early break, the path may not be the shortest one, but good enough for current scenario. 

		for (int i = 0; i < (int)Direction::SIZE_; ++i)
		{
			Vec2i p = node.pos + dir_vector_i[i];
			if (!IsTileWalkable(p))
				continue;
			Node new_node;
			float new_distance = GetAStarTile(node.pos).distance + 1;
			new_node.score = new_distance * new_distance + Vec2i::DistanceSquared(p, goal); // TODO: tweak score, currently heuristic is squared but distance is not
			AStarTile& prev_tile = GetAStarTile(p);
			if (new_distance < prev_tile.distance)
			{
				prev_tile.distance = new_distance;
				prev_tile.backtrack = node.pos;
				new_node.pos = p;
				//LOG_DEBUG("push {%d; %d} dist=%f; score=%f; ", new_node.pos.x, new_node.pos.y, new_distance, new_node.score);
				qu.push(new_node);
			}
		}
	}
	//LOG_DEBUG("Pathfinding nodes visited: %d", nodes_visited);

	if (GetAStarTile(goal).distance == INFINITY)
		return {}; // NO PATH FOUND

	{ // backtrack
		std::vector<Vec2i> path;
		path.reserve((int)GetAStarTile(goal).distance + 2);
		Vec2i p = goal;
		while (p != start)
		{
			path.push_back(p);
			p = GetAStarTile(p).backtrack;
		}
		//std::reverse(path.begin(), path.end());
		return path;
	}

}

Vec2 Level::FindEnemySpawnPoint()
{
	Vec2 best = Vec2(1, 1);
	for (int i = 0; i < 128; ++i)
	{
		Vec2i pos = { 1 + rand() % (size.x - 2), 1 + rand() % (size.y - 2) };
		if (IsTileWalkable(pos))
		{
			best = (Vec2)pos + Vec2(.5f);
			if (Vec2::DistanceSquared(player.pos, best) > (4 * 4))
				return best;
		}
	}
	return best;
}

Enemy& Level::CreateEnemy(Vec2 pos, Enemy::Type type)
{
	enemies.push_back({});
	enemies.back().pos = enemies.back().target_pos = pos;
	enemies.back().type = type;
	return enemies.back();
}

void Level::DestroyBlock(Vec2i pos)
{
	Vec2 pos_f = (Vec2)pos + Vec2(.5f);
	Tile t = GetTile(pos);
	SetTile(pos, Tile::FLOOR);
	if (t == Tile::POWERUP_SPEED)
		powerups.push_back({ Powerup::SPEED, pos_f});
	else if (t == Tile::POWERUP_RANGE)
		powerups.push_back({ Powerup::RANGE, pos_f });
	else if (t == Tile::POWERUP_BOMBS)
		powerups.push_back({ Powerup::BOMBS, pos_f });
}

void Level::KillPlayer()
{
	if (player.death_timer != INFINITY)
		return;
	player.death_timer = .6f;
}
