#include "texture.h"
#include "gfx.h"
#include <algorithm>


GLuint Texture::gBoundTexture[Texture::MAX_TEXTURE_UNITS] = { 0 };
GLuint Texture::gActiveTextureUnit = 0;

Texture::~Texture()
{
	if (texture > 0)
	{
		LOG_DEBUG("Destroy texture '%d'", texture);
		glDeleteTextures(1, &texture);
	}
}

void Texture::CreateTexture(Vec2i new_size)
{
	GL_DEBUG_CHECK();
	if (texture) *this = Texture(); // delete previous texture
	glGenTextures(1, &texture);
	Bind();
	int mode = GL_RGBA;
	size = new_size;
	if (size != Vec2i(0,0))
		glTexImage2D(GL_TEXTURE_2D, 0, mode, size.x, size.y, 0, mode, GL_UNSIGNED_BYTE, nullptr);
	GL_DEBUG_CHECK();
	SetFiltering(filtering);
}

void Texture::UploadTexture(RGBA* pixels, Vec2i new_size)
{
	if (!texture)
		CreateTexture(Vec2i(0,0));
	size = new_size;
	Bind();
	int mode = GL_RGBA;
	glTexImage2D(GL_TEXTURE_2D, 0, mode, size.x, size.y, 0, mode, GL_UNSIGNED_BYTE, pixels);
	SetFiltering(filtering);
	GL_DEBUG_CHECK();
}

void Texture::SetFiltering(std::string mode)
{
	std::transform(mode.begin(), mode.end(), mode.begin(), ::tolower);
	if (mode.empty() || mode == "none" || mode == "off") SetFiltering(None);
	else if (mode == "default") SetFiltering(DEFAULT_FILTERING);
	else if (mode == "smooth") SetFiltering(Smooth);
	else if (mode == "nearest") SetFiltering(Nearest);
	else if (mode == "bilinear") SetFiltering(Bilinear);
	else if (mode == "trilinear") SetFiltering(Trilinear);
	else LOG_WARNING("Unkown blending mode: %s", mode.c_str());
}

void Texture::SetFiltering(FilteringMode mode)
{
	Bind();
	filtering = mode;
	switch (filtering)
	{
	case (None):
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		break;

	case (Smooth):
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;

	case(Nearest):
		GenMipMaps();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		break;

	case (FilteringMode::Bilinear):
		GenMipMaps();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;

	case (FilteringMode::Trilinear):
	{
		GenMipMaps();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		break;
	}
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	GL_DEBUG_CHECK();
}

void Texture::GenMipMaps()
{
	if (!mip_maps)
	{
		mip_maps = true;
	}
	Bind();
	glGenerateMipmap(GL_TEXTURE_2D);
	GL_DEBUG_CHECK();
}

bool Texture::SetTextureUnit(unsigned index)
{
	if (index == gActiveTextureUnit)
		return false;
	gActiveTextureUnit = index;
	glActiveTexture(GL_TEXTURE0 + gActiveTextureUnit);
	return true;
}

bool Texture::Bind(GLuint texture)
{
	if (gBoundTexture[gActiveTextureUnit] == texture)
		return false;

	gBoundTexture[gActiveTextureUnit] = texture;
	glActiveTexture(GL_TEXTURE0 + gActiveTextureUnit);
	glBindTexture(GL_TEXTURE_2D, texture);
	return true;
}

void Texture::Unbind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
	gBoundTexture[gActiveTextureUnit] = 0;
}