#include "shader.h"
#include <memory>
#include <algorithm>
#include "common.h"
#include "gfx.h"

using namespace std;

GLuint Shader::gBoundProgram = 0;

Shader::~Shader()
{
	if (program > 0) glDeleteProgram(program);
}

GLuint Shader::CreateFromSource(const char* src, GLenum type)
{
	ASSERT(type == GL_VERTEX_SHADER || type == GL_FRAGMENT_SHADER);
	// Create the shader object 
	GLuint shader = glCreateShader(type);
	if (shader == 0)
	{
		LOG_ERROR("glCreateShader() failed!");
		return 0;
	}

	// Upload shader
	glShaderSource(shader, 1, &src, nullptr);

	// Compile shader
	LOG("Compiling %s shader...", (type == GL_VERTEX_SHADER ? "vertex" : "fragment"));
	GLint status = 0;
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{// Log error
		GLint infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen > 1)
		{
			unique_ptr<char> infoLOG(new char[sizeof(GLchar) * infoLen]);
			glGetShaderInfoLog(shader, infoLen, NULL, infoLOG.get());
			LOG("Shader Source:\n%s", src);
			LOG_ERROR("Error compiling shader:\n%s\n", infoLOG.get());
		}
		else
		{
			LOG_ERROR("Error compiling shader! Reason unknown.\n");
		}
		return 0;
	}
	return shader;
}

bool Shader::CompileShader(const char* vertex_shader, const char* fragment_shader)
{
	GLuint vs = CreateFromSource(vertex_shader, GL_VERTEX_SHADER);
	if (vs == 0)
		return false;
	GLuint fs = CreateFromSource(fragment_shader, GL_FRAGMENT_SHADER);
	if (fs == 0)
	{
		glDeleteShader(vs);
		return false;
	}
	// Create Shader Program
	program = glCreateProgram();
	if (program == 0)
	{
		LOG_ERROR("glCreateProgram() failed!");
		return false;
	}

	// attach shaders to program
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glDeleteShader(vs);
	glDeleteShader(fs);
	LOG("Shader linking...");
	{// Link
		GLint status;
		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &status);
		GL_DEBUG_CHECK();
		if (status == GL_FALSE)
		{
			GLint length;
			// get the program info LOG
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
			unique_ptr<char> infoLOG(new char[sizeof(GLchar) * length]);
			glGetProgramInfoLog(program, length, &status, infoLOG.get());

			// print an error message and the info LOG
			LOG_ERROR("Shader Program linking failed: %s\n", infoLOG.get());

			// delete the program
			glDeleteProgram(program);
			program = 0;
			return false;
		}
	}
	LOG("Shader created.");

	// Get uniforms and atributes
	{
		GLint count;
		// Attributes			
		glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &count);
		attribs.resize(count);
		for (int i = 0; i < count; ++i)
		{
			char name[32];
			int nameLen;
			glGetActiveAttrib(program, i, 32, &nameLen, &(attribs[i].size), &(attribs[i].type), name);
			attribs[i].name = string(name, nameLen);
			attribs[i].location = glGetAttribLocation(program, name);
			ASSERT(attribs[i].location != -1);
		}
		//sort(attributes.begin(), attributes.end(), [](const Attribute& a, const Attribute& b)->bool{return a.name < b.name;});
		// Uniforms
		glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &count);
		uniforms.resize(count);
		for (int i = 0; i < count; ++i)
		{
			char name[32];
			int nameLen;
			glGetActiveUniform(program, i, 32, &nameLen, &(uniforms[i].size), &(uniforms[i].type), name);
			uniforms[i].name = string(name, nameLen);
			uniforms[i].location = glGetUniformLocation(program, name);
			ASSERT(uniforms[i].location != -1);
		}
		//sort(uniforms.begin(), uniforms.end(), [](const Uniform& a, const Uniform& b)->bool{return a.name < b.name;});
	}

	GL_DEBUG_CHECK();
	return true;
}


GLint  Shader::GetUniformLocation(const std::string& name) const
{
	auto it = find(uniforms.begin(), uniforms.end(), name);
	if (it == uniforms.end())
	{
		return -1;
	}
	return it->location;
}

GLint Shader::GetAttributeLocation(const std::string& name) const
{
	auto it = find(attribs.begin(), attribs.end(), name);
	if (it == attribs.end())
	{
		return -1;
	}
	return it->location;
}

void Shader::Bind()
{
	if (program != gBoundProgram)
	{
		gBoundProgram = program;
		glUseProgram(program);
	}
}

void Shader::Unbind()
{
	gBoundProgram = 0;
	glUseProgram(0);
}

bool Shader::GetLinkStatus()
{
	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	return status != GL_FALSE;
}

bool Shader::Attribute::operator < (const std::string& str) const
{
	return name < str;
}

bool Shader::Uniform::operator < (const std::string& str) const
{
	return name < str;
}

bool Shader::Attribute::operator == (const std::string& str)const
{
	return name == str;
}

bool Shader::Uniform::operator == (const std::string& str) const
{
	return name == str;
}

const std::vector<Shader::Uniform>& Shader::GetUniforms() const
{
	return uniforms;
}

const std::vector<Shader::Attribute>& Shader::GetAttributes() const
{
	return attribs;
}
