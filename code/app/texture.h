#pragma once
#include <glad.h>
#include <string>
#include "common.h"

// TODO: implement move constructor and move assign
class Texture
{
public:
	enum FilteringMode : uint8_t
	{
		// without mipmaps
		None, 
		Smooth, 
		// mipmapped filters
		Nearest,
		Bilinear,
		Trilinear,
	};
private:
	static constexpr FilteringMode DEFAULT_FILTERING = Smooth;
	static constexpr unsigned MAX_TEXTURE_UNITS = 8;
	static unsigned gActiveTextureUnit;
	static GLuint gBoundTexture[MAX_TEXTURE_UNITS];

	GLuint texture = 0;
	Vec2i size = Vec2i(0);
	FilteringMode filtering = DEFAULT_FILTERING;
	bool mip_maps = false;

	void GenMipMaps();
public:
	Texture() = default;
	~Texture();

	void CreateTexture(Vec2i size = Vec2i(0,0));
	void UploadTexture(RGBA* pixels, Vec2i size);
	void SetFiltering(FilteringMode mode);
	void SetFiltering(std::string mode);
	FORCE_INLINE GLuint GetGLHandle() { return texture;  }

	static bool SetTextureUnit(unsigned index);
	static bool Bind(GLuint gl_texture);
	FORCE_INLINE static GLuint GetBoundTexture() { return gBoundTexture[gActiveTextureUnit]; }
	FORCE_INLINE bool Bind() { return Texture::Bind(texture); }
	static void Unbind();
	FORCE_INLINE Vec2i GetSize() { return size; }
};