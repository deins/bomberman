#include "app.h"
#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <fstream>
#include <glad.h>
#include "gfx.h"
#include "sprite.h"

#ifdef EMSCRIPTEN
void App::EmscriptenLoop()
{
	static uint64_t prev_frame = SDL_GetPerformanceCounter();

	uint64_t now = SDL_GetPerformanceCounter();
	float delta_t = (float)((now - prev_frame) / (double)SDL_GetPerformanceFrequency());
	if (delta_t > 1.0f / 15.0f)
		delta_t = 1.0f / 15.0f; // limit delta_t to avoid glitches
	prev_frame = now;
	gApp.ProcessFrame(delta_t);
}
#endif

App::~App() {
	if (is_init)
	{
		SDL_DestroyWindow(window);
		SDL_Quit();
	}
}

void App::Init()
{
	{	// SDL and OpenGL
		ASSERT(!is_init);
		is_init = true;
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
			FATAL_ERROR("Failed to init SDL\n");

#ifdef _DEBUG
		SDL_LogSetAllPriority(SDL_LOG_PRIORITY_DEBUG);
		LOG("DEBUG BUILD!");
#else
		SDL_LogSetAllPriority(SDL_LOG_PRIORITY_INFO);
		LOG("RELEASE BUILD!");
#endif

		window = SDL_CreateWindow("Bomberman", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			window_size.x, window_size.y, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_MAXIMIZED);
		if (!window)
			FATAL_ERROR("Can't create window!");

		// Setup opengl
		auto SetGlAttribute = [](SDL_GLattr attr, int val)
		{
			if (SDL_GL_SetAttribute(attr, val) != 0)
				FATAL_ERROR("SDL_GL_SetAttribute failed!");
		};
#ifdef EMSCRIPTEN
		SetGlAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
#else
#if	!defined(_DEBUG)
		// release for GL 2.1
		SetGlAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		SetGlAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SetGlAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
#else
		// Use opengl 3.2 when debugging to be able to use RenderDoc debugger
		SetGlAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SetGlAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
		SetGlAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
#endif
#endif
		gl_context = SDL_GL_CreateContext(window);
		if (!gl_context)
			FATAL_ERROR("Can't create GL context!");
		//SDL_GL_MakeCurrent(window, gl_context);

		if (SDL_GL_SetSwapInterval(1) != 0) // VSYNC ON
			LOG_ERROR("Can't set vsync!");

		gladLoadGLLoader(SDL_GL_GetProcAddress); // load opengl functions
		glGetError();
		
		LOG("OpenGL ready.");
		LOG("Vendor:   %s\n", glGetString(GL_VENDOR));
		LOG("Renderer: %s\n", glGetString(GL_RENDERER));
		LOG("Version:  %s\n", glGetString(GL_VERSION));
		GL_DEBUG_CHECK();
	}

	{	// ASSETS
		LOG("Loading assets");
		gSprites.LoadSpriteSheet("bomberman");
		bool ok = level.Load("levels/1.txt");
		if (!ok) FATAL_ERROR("Can't load level!");
	}
}

void App::Start()
{
	ASSERT(is_init);
	ASSERT(!is_running);
	is_running = true;
#ifndef EMSCRIPTEN
	MainLoop();
#else
	emscripten_set_main_loop(App::EmscriptenLoop, 0, 0);
#endif
}

#ifndef EMSCRIPTEN
void App::MainLoop()
{
	uint64_t prev_frame = SDL_GetPerformanceCounter();
	while (is_running)
	{
		uint64_t now = SDL_GetPerformanceCounter();
		float delta_t = (float)((now - prev_frame) / (double)SDL_GetPerformanceFrequency());
		if (delta_t > 1.0f / 15.0f) 
			delta_t = 1.0f / 15.0f; // limit delta_t to avoid glitches
		prev_frame = now;
		ProcessFrame(delta_t);
	}
	is_running = false;
}
#endif

void App::ProcessFrame(float delta_t)
{
	input.Update();
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			is_running = false;
			break;

		case SDL_WINDOWEVENT:
			switch (event.window.event)
			{
			case (SDL_WINDOWEVENT_RESIZED): {
				window_size.x = event.window.data1;
				window_size.y = event.window.data2;
				LOG("Window resize: %d %d", window_size.x, window_size.y);
			}
			case (SDL_WINDOWEVENT_FOCUS_GAINED):
				//SDL_ShowCursor(0);
				window_focus = true;
				break;
			case (SDL_WINDOWEVENT_FOCUS_LOST):
				SDL_ShowCursor(1);
				window_focus = false;
				break;
			}
			break;

		default:
			input.ProcessEvent(event);
			break;
		}
	}
	if (input.IsKeyDown(SDL_SCANCODE_ESCAPE))
		is_running = false;

	gGfx.BeginFrame();
	OnEachFrame(delta_t);
	gGfx.EndFrame();
	SDL_GL_SwapWindow(window);
}

void App::OnEachFrame(float delta_t)
{
	level.OnEachFrame(delta_t);
	level.Draw();
}

//#ifdef EMSCRIPTEN
//#include <sys/types.h>
//#include <dirent.h>
//#endif

std::string App::GetAsset(std::string path)
{
#ifdef EMSCRIPTEN
	//DIR* dir = opendir("./");
	//struct dirent *entry;
	//while ((entry = readdir(dir)) != NULL)
	//	LOG("dir:  %s\n", entry->d_name);
	std::ifstream f(path, std::ios::binary);
#else
	std::ifstream f("data/" + path, std::ios::binary);
#endif
	if (!f)
		FATAL_ERROR("Can't load file '%s'!", path.c_str());
	f.seekg(0, std::ios::end);
	size_t size = (size_t)f.tellg();
	std::string buffer(size, '\0');
	f.seekg(0);
	f.read(&buffer[0], size);
	LOG("file '%s' loaded", path.c_str());
	return buffer;
}
