#ifdef EMSCRIPTEN
#define FS_HEADER "precision mediump float;\n"
#define VS_HEADER "precision lowp float;\n"
#else
#define FS_HEADER "#version 120\n"
#define VS_HEADER "#version 120\n"
#endif

const char default_shader_vs[] = 
VS_HEADER
R"(
		attribute vec3 Position; 
		attribute vec4 Color; 
		attribute vec2 Uv; 

		varying vec4 vertex_color; 
		varying vec2 uv;

		uniform mat4 MVP;

		void main()	
		{
			vertex_color = Color;
			uv = Uv;
 			gl_Position = MVP * vec4(Position, 1.0);
		}
	)";

const char default_shader_fs[] = 
FS_HEADER
R"(
		uniform sampler2D TEXTURE_0;

		varying vec4 vertex_color; 
		varying vec2 uv; 

		void main()
		{
			vec4 tex_col = texture2D( TEXTURE_0, uv);
			gl_FragColor = tex_col * vertex_color * 2.0;
			//gl_FragColor += vec4(uv, 0.0, 1.0);
			//gl_FragColor = vertex_color;
		}
	)";