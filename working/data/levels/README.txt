Levels are simple text documents, first line contains 2 integers seperated by space, level size: rows columns
Starting from second line level is described. It should contain rows * columns (excluding newlines)
Legend:
X - Wall (indestructable)
B - Explodable block
. - Walkable tile (floor)
P - Floor with player spawn point
+ - Wandering enemy
* - Following enemy
