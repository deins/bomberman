cmake_minimum_required(VERSION 3.9)
enable_language(C CXX)
project(bomb)

set(CMAKE_CXX_STANDARD 17)

if (CMAKE_SYSTEM_NAME STREQUAL "Emscripten")
	set(EMSCRIPTEN 1)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/working/web")
	set(CMAKE_EXECUTABLE_SUFFIX ".html")
	set(CMAKE_CXX_FLAGS_DEBUG  ${CMAKE_CXX_FLAGS_DEBUG} -g4 -s ASSERTIONS=1)
	set(CMAKE_EXE_LINKER_FLAGS_DEBUG  ${CMAKE_EXE_LINKER_FLAGS_DEBUG} -g4 -s ASSERTIONS=1)
	set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-Wno-writable-strings")

	# -s STACK_OVERFLOW_CHECK=1 -s SAFE_HEAP=1 
	set(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} "-s USE_SDL=2 --preload-file ${CMAKE_SOURCE_DIR}/working/data@/ -s TOTAL_MEMORY=234881024 -s \"BINARYEN_TRAP_MODE='clamp'\" -Wno-writable-string")
	#-s BINARYEN_METHOD='interpret-binary'
	if (WASM)
		message(STATUS "WASM: ENABLED")

		#set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-s WASM=1 -s 'BINARYEN_METHOD=\"native-wasm,asmjs\"'")
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -s WASM=1") #-s \"BINARYEN_TRAP_MODE='js'\"
	else ()
		message(STATUS "WASM: DISABLED")
	endif()
else()
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/working/bin")
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG "${CMAKE_SOURCE_DIR}/working/bin")
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_SOURCE_DIR}/working/bin")
endif()

if (MSVC)
	#message(STATUS "msvc configuration")
	#add_compile_options(/arch:AVX2)
	add_compile_options(/fp:fast)
	add_compile_options(/MP)
	add_compile_options("$<$<CONFIG:DEBUG>:/Ob1>")
	#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS")

	foreach(flag_var
        CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO)
      if(${flag_var} MATCHES "/MD")
        string(REGEX REPLACE "/MD" "/MT" ${flag_var} "${${flag_var}}") 	# force static vc runtimes
      endif()
	  	STRING (REGEX REPLACE "/RTC(su|[1su])" "" ${flag_var} "${${flag_var}}") # remove runtime checks (otherwise debug build performance is too slow)
	endforeach(flag_var)
elseif (CMAKE_CXX_COMPILER_ID  MATCHES "GNU")
	#add_compile_options(-mavx2)
	add_compile_options(-msse2)
endif()

add_subdirectory("code")

# set msvc startup project
set_property(DIRECTORY ${CMAKE_SOURCE_DIR}/code/app PROPERTY VS_STARTUP_PROJECT BOMBERMAN) 
